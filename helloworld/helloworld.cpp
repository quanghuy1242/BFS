// helloworld.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
using namespace std;

class Box
{
public:
	float getVolume();
	float getCao(float cao);
	void setCao(float cao);
	float getRong(float rong);
	void setRong(float rong);
	float getDay(float day);
	void setDay(float day);

protected:
	float cao;
	float rong;
	float day;
};

float Box::getVolume()
{
	return this->cao * this->day * this->rong;
}

float Box::getCao(float cao) {
	return cao;

}
void Box::setCao(float cao) {
	this->cao = cao;
}

float Box::getRong(float rong) {
	return rong;

}
void Box::setRong(float rong) {
	this->rong = rong;
}

float Box::getDay(float day) {
	return day;

}
void Box::setDay(float day) {
	this->day = day;
}

class Boxcon:public Box //Kế thừa
{
public:

private:

};

int main()
{
	Box box;
	box.setCao(2);
	box.setRong(3);
	box.setDay(4);
	cout << "Vomlume = " << box.getVolume();
}
