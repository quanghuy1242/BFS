#include "pch.h"
#include <iostream>
#include <queue>
#define ROW 9
#define COL 10

using namespace std;

struct Point {
	int x;
	int y;
};

struct MyQueue {
	Point pt;
	int path_cost;
};

int rowNum[] = { -1, 0, 0, 1 };
int colNum[] = { 0, -1, 1, 0 };

bool isValid(int row, int col);
int Breadth_First_Search(int DT[][COL], Point init, Point dest);

int main() {
	int DT[ROW][COL] =
	{
		{ 1, 0, 1, 1, 1, 1, 0, 1, 1, 1 },
		{ 1, 0, 1, 0, 1, 1, 1, 0, 1, 1 },
		{ 1, 1, 1, 0, 1, 1, 0, 1, 0, 1 },
		{ 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 },
		{ 1, 1, 1, 0, 1, 1, 1, 0, 1, 0 },
		{ 1, 0, 1, 1, 1, 1, 0, 1, 0, 0 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 1, 1, 1, 1, 0, 1, 1, 1 },
		{ 1, 1, 0, 0, 0, 0, 1, 0, 0, 1 }
	};

	Point init = { 0, 0 };
	Point dest = { 3, 4 };

	int path_cost = Breadth_First_Search(DT, init, dest);

	if (path_cost != -1) {
		cout << "Path Cost is " << path_cost;
	}
	else {
		cout << "404 Error";
	}

	return 0;
}

bool isValid(int row, int col) {
	if ((row >= 0) && (row < ROW) && (col >= 0) && (col < COL)) return true;
	return false;
}


int Breadth_First_Search(int DT[][COL], Point init, Point dest) {
	if (!DT[init.x][init.y] || !DT[dest.x][dest.y]) return -1;

	//tao mang danh dau, va gan toan bo mang la false (vi chua co diem nao di duoc explore)
	bool explored[ROW][COL];
	memset(explored, false, sizeof explored);

	//danh dau diem init la da explore
	explored[init.x][init.y] = true;

	//tao queue chua
	queue<MyQueue> q;

	//Ban dau chi co diem init va path_cost la 0
	MyQueue s = { init, 0 };
	q.push(s);

	while (!q.empty()) {
		//Lay diem dau tien trong queue ra
		MyQueue curr = q.front();
		Point pt = curr.pt;
		cout << pt.x << " " << pt.y << endl;

		//Neu diem pt nay ma trung voi diem dest, thi thoi
		if (pt.x == dest.x && pt.y == dest.y) return curr.path_cost;

		//Neu pt chua phai la diem ket thuc thi deq no va xet de enq cac diem lan can
		q.pop();

		//xet cac diem lan can
		for (int i = 0; i < 4; i++) {
			//tinh diem lan can thu i
			int row = pt.x + rowNum[i];
			int col = pt.y + colNum[i];

			//Neu diem lan can vua tinh co duong di, va chua explore thi enq no
			if (isValid(row, col) && DT[row][col] && !explored[row][col]) {
				explored[row][col] = true;
				MyQueue nowq = { {row, col}, curr.path_cost + 1 };
				q.push(nowq);
			}
		}
	}
	return -1; //Neu tim ma khong thay duong di thi return -1
}